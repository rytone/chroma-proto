// Code generated by protoc-gen-go.
// source: handshake.proto
// DO NOT EDIT!

package proto

import proto1 "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto1.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type ShardInit struct {
	Version string `protobuf:"bytes,1,opt,name=version" json:"version,omitempty"`
}

func (m *ShardInit) Reset()                    { *m = ShardInit{} }
func (m *ShardInit) String() string            { return proto1.CompactTextString(m) }
func (*ShardInit) ProtoMessage()               {}
func (*ShardInit) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{0} }

func (m *ShardInit) GetVersion() string {
	if m != nil {
		return m.Version
	}
	return ""
}

type BackboneResponse struct {
	ShardId    int32  `protobuf:"varint,1,opt,name=shard_id,json=shardId" json:"shard_id,omitempty"`
	ShardCount int32  `protobuf:"varint,2,opt,name=shard_count,json=shardCount" json:"shard_count,omitempty"`
	BackboneId string `protobuf:"bytes,3,opt,name=backbone_id,json=backboneId" json:"backbone_id,omitempty"`
}

func (m *BackboneResponse) Reset()                    { *m = BackboneResponse{} }
func (m *BackboneResponse) String() string            { return proto1.CompactTextString(m) }
func (*BackboneResponse) ProtoMessage()               {}
func (*BackboneResponse) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{1} }

func (m *BackboneResponse) GetShardId() int32 {
	if m != nil {
		return m.ShardId
	}
	return 0
}

func (m *BackboneResponse) GetShardCount() int32 {
	if m != nil {
		return m.ShardCount
	}
	return 0
}

func (m *BackboneResponse) GetBackboneId() string {
	if m != nil {
		return m.BackboneId
	}
	return ""
}

func init() {
	proto1.RegisterType((*ShardInit)(nil), "chroma.ShardInit")
	proto1.RegisterType((*BackboneResponse)(nil), "chroma.BackboneResponse")
}

func init() { proto1.RegisterFile("handshake.proto", fileDescriptor1) }

var fileDescriptor1 = []byte{
	// 167 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0xcf, 0x48, 0xcc, 0x4b,
	0x29, 0xce, 0x48, 0xcc, 0x4e, 0xd5, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x4b, 0xce, 0x28,
	0xca, 0xcf, 0x4d, 0x54, 0x52, 0xe5, 0xe2, 0x0c, 0xce, 0x48, 0x2c, 0x4a, 0xf1, 0xcc, 0xcb, 0x2c,
	0x11, 0x92, 0xe0, 0x62, 0x2f, 0x4b, 0x2d, 0x2a, 0xce, 0xcc, 0xcf, 0x93, 0x60, 0x54, 0x60, 0xd4,
	0xe0, 0x0c, 0x82, 0x71, 0x95, 0xf2, 0xb9, 0x04, 0x9c, 0x12, 0x93, 0xb3, 0x93, 0xf2, 0xf3, 0x52,
	0x83, 0x52, 0x8b, 0x0b, 0xf2, 0xf3, 0x8a, 0x53, 0x85, 0x24, 0xb9, 0x38, 0x8a, 0x41, 0x5a, 0xe3,
	0x33, 0x53, 0xc0, 0xca, 0x59, 0x83, 0xd8, 0xc1, 0x7c, 0xcf, 0x14, 0x21, 0x79, 0x2e, 0x6e, 0x88,
	0x54, 0x72, 0x7e, 0x69, 0x5e, 0x89, 0x04, 0x13, 0x58, 0x96, 0x0b, 0x2c, 0xe4, 0x0c, 0x12, 0x01,
	0x29, 0x48, 0x82, 0x9a, 0x07, 0xd2, 0xce, 0x0c, 0xb6, 0x8d, 0x0b, 0x26, 0xe4, 0x99, 0xe2, 0xc4,
	0x1e, 0xc5, 0x0a, 0x76, 0x68, 0x12, 0x1b, 0x98, 0x32, 0x06, 0x04, 0x00, 0x00, 0xff, 0xff, 0x2a,
	0x1b, 0x7b, 0x9f, 0xc2, 0x00, 0x00, 0x00,
}
